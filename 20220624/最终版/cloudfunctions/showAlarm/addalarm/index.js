const cloud = require('wx-server-sdk');

cloud.init({
  env: cloud.DYNAMIC_CURRENT_ENV
});
const db = cloud.database();

// 修改数据库信息云函数入口函数
exports.main = async (event, context) => {
  try {
    // 遍历修改数据库信息
    await db.collection('alarm_history').add({
      // data 字段表示需新增的 JSON 数据
      data: {
        date: event.date,
        time: event.time,
        duration: event.duration,
        id:event.id,
      }
    });
    return {
      success: true,
      //date:event.date,
      //time: event.time,
      //duration: event.duration,
      //data: event.data
    };
  } catch (e) {
    return {
      success: false,
      errMsg: e
    };
  }
};
