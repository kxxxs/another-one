// 云函数入口文件
const getalarm = require('./getalarm/index');
const addalarm = require('./addalarm/index');
const deletealarm = require('./deletealarm/index');

// 云函数入口函数
exports.main = async (event, context) => {
  switch (event.type) {
    case 'addalarm':
      return await addalarm.main(event, context);
    case 'getalarm':
      return await getalarm.main(event, context);
     case 'deletealarm':
       return await deletealarm.main(event, context);
    // case 'selectRecord':
    //   return await selectRecord.main(event, context);
    // case 'updateRecord':
    //   return await updateRecord.main(event, context);
    // case 'sumRecord':
    //   return await sumRecord.main(event, context);
  }
}