const cloud = require('wx-server-sdk');

cloud.init({});
const db = cloud.database();

// 查询数据库集合云函数入口函数
exports.main = async (event, context) => {
  // 返回数据库查询结果
  return await db.collection('alarm_history').get();
};
