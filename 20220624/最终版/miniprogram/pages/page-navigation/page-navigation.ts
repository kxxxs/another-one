// pages/page-navigation/page-navigation.ts
Page({
  data: {
    // loaded: false,
  },
  toClock(){
    wx.navigateTo({
      url:'../page1/page1',
      success: function() {
      //console.log(res)
      // 通过eventChannel向被打开页面传送数据
      // res.eventChannel.emit('acceptDataFromOpenerPage', { data: 'test' })
      }
    })
  },
  sendWords(){
    wx.navigateTo({
      url:'../page2/page2',
      success: function() {

      }
    })
  },
  recordSound(){
    wx.navigateTo({
      url:'../page3/page3',
      success: function() {

      }
    })
  },
  chooseSong(){
    wx.navigateTo({
      url:'../page4/page4',
      success: function() {

      }
    })
  },
  shareLife(){
    wx.navigateTo({
      url:'../page5/page5',
      success: function() {

      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad() {
    // var that=this
    // wx.loadFontFace({
    //   family: 'Bitstream Vera Serif Bold',
    //   source: 'url("https://sungd.github.io/Pacifico.ttf")',
    //   success:(){
    //     that.setData({ loaded: true })
    //   },
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})