Component({
    data: {
      statusBarHeight: wx.getSystemInfoSync().statusBarHeight,
      left: wx.getSystemInfoSync().windowWidth - 17,
      menuList: [{
        title: 'bilibili',
        image:"http://img2.3png.com/c85909aef3e8ede354112afef77f8124d218.png",
        content:'XXX',
       }, 
      //{
      //   title: 'XXX',
      //   image:"https://tse1-mm.cn.bing.net/th/id/R-C.df09ce54f8b98950784e7bfd37d781c3?rik=cc4qN554RFk9rg&riu=http%3a%2f%2fimg.ivsky.com%2fimg%2ftupian%2fpre%2f201011%2f25%2fwurendianyingyuan-006.jpg&ehk=SsWy3dujG2sP87v1M0%2fIhvg1s4FXVRfdCszqdGYTsW4%3d&risl=&pid=ImgRaw&r=0",
      //   content:'XXX',
      // }, 
      {
        title: 'QQ音乐',
        image:"https://ts1.cn.mm.bing.net/th/id/R-C.6482dc3c3117e56b0380444fe8d831b0?rik=9UqlGCu8H7FaKQ&riu=http%3a%2f%2fp1.qhimg.com%2ft01d215e4945bc4f961.png&ehk=mOIO9dtowK3yfp35W3aqndXAeohCXUtjmEYtdVsQvw0%3d&risl=&pid=ImgRaw&r=0&sres=1&sresct=1",
        content:'也可转去网易云',
      },
      //{
      //   title: 'XXX',
      //   image:"https://tse1-mm.cn.bing.net/th/id/R-C.ff6fcbf5b0e1025092ee345a336ceeb0?rik=eRsC7vGGRDC%2fFw&riu=http%3a%2f%2fimg.dijiu.com%2f2017%2f0818%2f20170818044358360.png&ehk=%2b2i8Z9Mj9WDW63au3nsqrlSmOui%2f6enP85jrzoCaYyg%3d&risl=&pid=ImgRaw&r=0",
      //   content:'XX',
      // }, 
    ],
      },

      pageLifetimes: {
      show() {
        if (this._lastScrollLeft > 210 * this.data.menuList.length - wx.getSystemInfoSync().windowWidth) {
          this.setData({
            scrollLeft: 210 * this.data.menuList.length - wx.getSystemInfoSync().windowWidth,
          })
        }
      },
    },

    ready() {
      this._animate()
    },

    methods: {
      _animate() {
        wx.createSelectorQuery().select('#scroller').fields({
          scrollOffset: true,
          size: true,
        }, (res) => {
          this.animate('.avatar', [{
            borderRadius: '0',
            borderColor: 'red',
            transform: 'scale(1) translateY(-20px)',
            offset: 0,
          }, {
            borderRadius: '25%',
            borderColor: 'blue',
            transform: 'scale(.65) translateY(-20px)',
            offset: .5,
          }, {
            borderRadius: '50%',
            borderColor: 'blue',
            transform: `scale(.3) translateY(-20px)`,
            offset: 1
          }], 2000, {
            scrollSource: '#scroller',
            timeRange: 2000,
            startScrollOffset: 0,
            endScrollOffset: 85,
          })
  
          this.animate('.nickname', [{
            transform: 'translateY(0)',
          }, {
            transform: `translateY(${-44 - this.data.statusBarHeight}px)`,
          }], 1000, {
            scrollSource: '#scroller',
            timeRange: 1000,
            startScrollOffset: 120,
            endScrollOffset: 200,
          })
  
          this.animate('.search_input', [{
            opacity: '0',
            width: '0%',
          }, {
            opacity: '1',
            width: '100%',
          }], 1000, {
            scrollSource: '#scroller',
            timeRange: 1000,
            startScrollOffset: 120,
            endScrollOffset: 252
          })
  
          this.animate('.search_icon', [{
            right: '0',
            transform: 'scale(1)',
          }, {
            right: (wx.getSystemInfoSync().windowWidth * .5 - 20) + 'px',
            transform: 'scale(.6)',
          }], 1000, {
            scrollSource: '#scroller',
            timeRange: 1000,
            startScrollOffset: 140,
            endScrollOffset: 252,
          })
        }).exec()
  
        wx.createSelectorQuery().select("#scroller2").fields({
          scrollOffset: true,
          size: true,
        }, (res) => {
          // 绑定滚动元素
          const scrollTimeline = {
            scrollSource: '#scroller2',
            orientation: 'horizontal',
            timeRange: 1000,
            startScrollOffset: (210 * this.data.menuList.length - res.width) + 20,
            endScrollOffset: res.scrollWidth - res.width,
          }
          this.animate('#transform', [{
            offset: 0,
            width: '0px',
          }, {
            offset: 1,
            width: '30px',
          }], 1000, scrollTimeline)
        }).exec()
      },
      scroll(e) {
        if (e.detail.scrollLeft + wx.getSystemInfoSync().windowWidth + 3 >= e.detail.scrollWidth) {
          if (e.detail.deltaX < 0 && !this._active) {
            this._active = true
            this.setData({wording: '释放跳转'})
            wx.vibrateShort()
          } else if (e.detail.deltaX > 0) {
            this._active = false
            this.setData({ wording: '查看更多' })
          }
        } else {
          this._active = false
        }
        this._lastScrollLeft = e.detail.scrollLeft
        clearTimeout(this._timer)
      },
      // touchend() {
      //   clearTimeout(this._timer)
      //   if (this._active) {
      //     wx.navigateTo({
      //       url: '/second/index',
      //     })
      //     this._active = false
      //   } else if (this._lastScrollLeft > 210 * this.data.menuList.length - wx.getSystemInfoSync().windowWidth) {
      //     this.setData({
      //       scrollLeft: 210 * this.data.menuList.length - wx.getSystemInfoSync().windowWidth,
      //     })
      //   }
      // }
    },
  })
  