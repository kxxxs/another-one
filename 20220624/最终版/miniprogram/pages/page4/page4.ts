
Page({
    data: {
      ipt:"",
      hasmusic:false,
      musicInfo:"",
    },

    choosemusic(){
        let that=this
        wx.chooseMessageFile({
          count: 1,
          success: function (res){
            //console.log(res)
            let path=res.tempFiles[0]
            that.setData({
              tempFilePaths:path.path,
              hasmusic:true,
              musicInfo:path.name,
            })
            //console.log(that.data.hasmusic)
          },
          fail: function (e) {
            console.log('选择图片失败', e)
          }
        })
      },
      cancle(){
        this.setData({
          hasmusic:false,
          tempFilePaths: '',
        })
      },
      
    send(){
      var that=this
      wx.request({
        url:"https://openapi.heclouds.com/application?action=CreateDeviceFile&version=1",
        method:'POST',
        header:{
          'Content-type':'multipart/form-data',
          'authorization': 'version=2020-05-29&res=userid%2F253982&et=1655551065&method=sha1&sign=RhuiPnjchAVzHTw0eBvb%2FRVBfB8%3D'
        },
        data:{
          "device_name": "test",
          "product_id": "AYAjgUtWZB",
          "file": that.data.ipt
        },
        success(res){
               console.log("更新数据成功",res)
             },
             fail: function(){
              //wx.showToast({ title: '上传错误' })
            },
            complete:function(){
              wx.hideLoading()
            }
      })
    },
  
    onLoad() {
  
    },
  
    onReady() {
  
    },
  
    onShow() {
  
    },
  
    onHide() {
  
    },
  
    onUnload() {
  
    },
  
    onPullDownRefresh() {
  
    },
  
    onReachBottom() {
  
    },
  
    onShareAppMessage() {
  
    }
  })