const recorderManager = wx.getRecorderManager()
const innerAudioContext = wx.createInnerAudioContext()
Page({
    data: {
      ipt:"",
      hasrecord:false,
      recordInfo:"",
      status:false,
      tempFilePath:'',
      fileSize:0,
      duration:0,
    },

    start: function () {
      var that=this
      const options = {
        duration: 120000,//指定录音的时长，单位 ms
        sampleRate: 16000,//采样率
        numberOfChannels: 1,//录音通道数
        encodeBitRate: 96000,//编码码率
        format: 'mp3',//音频格式，有效值 aac/mp3
        frameSize: 50,//指定帧大小，单位 KB
      }
      wx.authorize({
        scope: 'scope.record',
        success() {
          console.log("录音授权成功");
          that.setData({
            status: true,
          })
          recorderManager.start(options);
          recorderManager.onStart(() => {
            console.log('recorder start')
          });
          recorderManager.onError((res) => {
            console.log(res);
          })
        },
        fail() {
          console.log("第一次录音授权失败");
          wx.showModal({
            title: '提示',
            content: '您未授权录音，功能将无法使用',
            showCancel: true,
            confirmText: "授权",
            confirmColor: "#52a2d8",
            success: function (res) {
              if (res.confirm) {
                //确认则打开设置页面
                wx.openSetting({
                  success: (res) => {
                    console.log(res.authSetting);
                    if (!res.authSetting['scope.record']) {
                      //未设置录音授权
                      console.log("未设置录音授权");
                      wx.showModal({
                        title: '提示',
                        content: '您未授权录音，功能将无法使用',
                        showCancel: false,
                        success: function (res) {
                        },
                      })
                    } else {
                      //第二次才成功授权
                      console.log("设置录音授权成功");
                      that.setData({
                        status: true,
                      })
                      recorderManager.start(options);
                      recorderManager.onStart(() => {
                        console.log('recorder start')
                      });
                      //错误回调
                      recorderManager.onError((res) => {
                        console.log(res);
                      })
                    }
                  },
                  fail: function () {
                    console.log("授权设置录音失败");
                  }
                })
              } else if (res.cancel) {
                console.log("cancel");
              }
            },
            fail: function () {
              console.log("openfail");
            }
          })
        }
      })
    },
    
    pause: function () {
      recorderManager.pause();
      recorderManager.onPause(() => {
        console.log('暂停录音')
      })
    },
    resume: function () {
      recorderManager.resume();
      recorderManager.onStart(() => {
        console.log('重新开始录音')
      });
      recorderManager.onError((res) => {
        console.log(res);
      })
    },

    stop: function () {
      recorderManager.stop();
      recorderManager.onStop((res) => {
        this.setData({
          tempFilePath:res.tempFilePath,
          duration:res.duration,
          fileSize:res.fileSize,
          hasrecord:true,
          status:false,
        })
        console.log('停止录音', res)
      })
    },

    play: function () {
      innerAudioContext.autoplay = true
      innerAudioContext.src = this.data.tempFilePath,
      innerAudioContext.onPlay(() => {
        console.log('开始播放')
      })
      innerAudioContext.onError((res) => {
        console.log(res)

      })
    },

    cancle(){
      this.setData({
        tempFilePath:'',
          duration:0,
          fileSize:0,
          hasrecord:false,
          status:false,
      })
    },

    send(){
      var that=this
      wx.cloud.uploadFile({
        cloudPath:'record.mp3',
        filePath:that.data.tempFilePath,
        success: res => {
          // get resource ID
          console.log('成功发送到云上',res.fileID)
        },
        fail: err => {
          console.log('上传至云失败',err)
        }
      })

     
    },
  
    onLoad() {
  
    },
  
    onReady() {
  
    },
  
    onShow() {
  
    },
  
    onHide() {
  
    },
  
    onUnload() {
  
    },
  
    onPullDownRefresh() {
  
    },
  
    onReachBottom() {
  
    },
  
    onShareAppMessage() {
  
    }
  })