// pages/page1/page1.ts
const devicesId = "替换为你的设备ID" // 填写在OneNet上获得的devicesId 形式就是一串数字 例子:9939133
const api_key = "replace with your api-key" // 填写在OneNet上的 api-key 例子: VeFI0HZ44Qn5dZO14AuLbWSlSlI=
Page({
  data: {
    hasmusic:false,
    haveclock:false,
    tempFilePaths: '',
    date:"2022-06-17",
    time:"08:30",
    duration:'',
    musicInfo:"",
    clock_list:[],
    lists:[],
    // {date:"", time:"" , duration:''}
    envId:'',
    // year:"2022",
    // month:"06",
    // day:"17",
    // hour:"",
    // minute:"",
  },

  onLoad(options) {
    this.setData({
      envId: options.envId
    });
    wx.cloud.callFunction({
      name:'showAlarm',
      config:{
        env: this.data.envId
      },
      data:{
        type:'getalarm'
      }
    }).then((resp) => {
      //console.log(resp.result)
      if(resp.result.data.length==0){      //该处报错不影响正常功能，应该是语法问题
        this.setData({
          haveclock:false
        })
      }
      else{
        this.setData({
          clock_list:resp.result.data,   //该处报错不影响正常功能，应该是语法问题
          haveclock:true
        })
        //console.log(Number(this.data.clock_list[this.data.clock_list.length-1].id)+1)
      };
      wx.hideLoading();
    }).catch((e) => {
      console.log(e);
      wx.hideLoading();
    });
  },

  choosemusic(){
    let that=this
    wx.chooseMessageFile({
      count: 1,
      success: function (res){
        //console.log(res)
        let path=res.tempFiles[0]
        that.setData({
          tempFilePaths:path.path,
          hasmusic:true,
          musicInfo:path.name,
        })
        //console.log(that.data.hasmusic)
      },
      fail: function (e) {
        console.log('选择图片失败', e)
      }
    })
  },
  cancle(){
    this.setData({
      hasmusic:false,
      tempFilePaths: '',
    })
  },

  dateChange:function(e:any){
    this.setData({
      date:e.detail.value
    })
  },

  timeChange:function(e:any){
    this.setData({
      time:e.detail.value
    })
  },

  durationSet:function(e:any){
    this.setData({
      duration:e.detail.value
    })
  },

  input(){
   
    wx.cloud.callFunction({
      name:'showAlarm',
      config:{
        env: this.data.envId
      },
      data:{
        type:'addalarm',
        date:this.data.date,
        time:this.data.time,
        duration:this.data.duration,
        id:Number(this.data.clock_list[this.data.clock_list.length-1].id)+1,
      }
    }).then(() => {
      //console.log(resp)
      // this.setData({
      //   clock_list:resp.result?.padStart,
      // });
      wx.hideLoading();
    }).catch((e) => {
      console.log(e);
      wx.hideLoading();
    });
    
    wx.request({
      url:"https://openapi.heclouds.com/application?action=CreateDeviceFile&version=1",
      method:'POST',
      header:{
        'Content-type':'multipart/form-data',
        'authorization': 'version=2020-05-29&res=userid%2F253982&et=1655551065&method=sha1&sign=RhuiPnjchAVzHTw0eBvb%2FRVBfB8%3D'
      },
      data:{
        "device_name": "test",
        "product_id": "AYAjgUtWZB",
        "file": '123'
      },
      success(res){
          console.log("更新数据成功",res)
          },
          fail: function(){
             wx.showToast({ title: '上传错误' })
          },
          complete:function(){
            wx.hideLoading()
          }
    })
    
  },

  delete(e:any){
    console.log(e)
    let id= e.currentTarget.dataset.id+1;
    //if(id==1){} //写出一个toast说明id=1时不可删除
    //else{}      //id不为1则执行云函数删除。
    wx.cloud.callFunction({
      name:'showAlarm',
      config:{
        env: this.data.envId
      },
      data:{
        type:'deletealarm',
        id:id,
      }
    }).then((resp) => {
      console.log(resp)
      if(resp.result.data.length==0){      //该处报错不影响正常功能，应该是语法问题
        this.setData({
          haveclock:false
        })
      }
      else{
        this.setData({
          clock_list:resp.result.data,   //该处报错不影响正常功能，应该是语法问题
          haveclock:true
        })
        //console.log(Number(this.data.clock_list[this.data.clock_list.length-1].id)+1)
      };
      wx.hideLoading();
    }).catch((e) => {
      console.log(e);
      wx.hideLoading();
    });
  },

  onReady() {

  },

  onShow() {

  },

  onHide() {

  },

  onUnload() {

  },

  onPullDownRefresh() {

  },

  onReachBottom() {

  },

  onShareAppMessage() {

  }
})